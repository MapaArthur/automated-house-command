package com.automated.house.command.repository;

import com.automated.house.command.domain.model.dto.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {

    @Query(value = "select * from user_house where cpf = :cpf and password = :password", nativeQuery = true)
    User findByCpfPassword(@Param("cpf")String cpf, @Param("password")String password);

    @Query(value = "delete * from user_house where cpf = :cpf", nativeQuery = true)
    void deleteByCpf(@Param("cpf") String cpf);

    @Query(value = "select * from user_house where cpf = :cpf", nativeQuery = true)
    Optional<User> findByCpf(@Param("cpf") String cpf);
}
