package com.automated.house.command.domain.model.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Entity(name = "residence")
public class Residence {
    @Id
    @Column(nullable = false, length = 50)
    private Long id;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(nullable = false, length = 50)
    private String state;
    @Column(nullable = false, length = 50)
    private String city;
    @Column(nullable = false, length = 50)
    private String district;
    @Column(nullable = false, length = 50)
    private String road;
    @Column(nullable = false, length = 50)
    private String cpf;
}
