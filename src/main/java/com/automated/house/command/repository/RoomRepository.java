package com.automated.house.command.repository;

import com.automated.house.command.domain.model.dto.Residence;
import com.automated.house.command.domain.model.dto.Room;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoomRepository extends CrudRepository<Room, Long> {

    @Query(value = "select * from room where id_residence = :id ", nativeQuery = true)
    List<Room> findByIdResidence(Long id);
}
