package com.automated.house.command.controller;

import com.automated.house.command.domain.model.dto.User;
import com.automated.house.command.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "https://docenteprotagonista.com.br")
@RestController
public class UserControllerImpl {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/api/user/{cpf}")
    public ResponseEntity getUser(@PathVariable("cpf") String cpf){
        return userRepository.findById(cpf)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/api/user/create")
    public User postUser(@RequestBody User usuario)
    {
        return userRepository.save(usuario);
    }

    @DeleteMapping("/api/user/delete/{cpf}")
    public ResponseEntity deleteUser(@PathVariable("cpf") String cpf)
    {
        try{
            userRepository.deleteById(cpf);
            return ResponseEntity.ok().build();
        }
        catch (Exception ex)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/api/user/update")
    public ResponseEntity updateUser(@RequestBody User usuario)
    {
        try{
            var user = userRepository.findById(usuario.getCpf());
            if(!user.isPresent())
            {
                return ResponseEntity.noContent().build();
            }
            else{
                user.get().setName(usuario.getName());
                user.get().setPassword(usuario.getPassword());
                userRepository.save(user.get());
            }
            return ResponseEntity.ok().body(user);
        }
        catch (Exception ex)
        {
            return ResponseEntity.internalServerError().build();
        }
    }
}