package com.automated.house.command.domain.model.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "accessory")
public class Accessory {
    @Id
    @Column(nullable = false, length = 50)
    private Long id;
    @Column(nullable = false, length = 50)
    private String name;
    @Column(nullable = false, length = 50)
    private Long idRoom;
}
