package com.automated.house.command.controller;

import com.automated.house.command.domain.model.dto.Residence;
import com.automated.house.command.domain.model.dto.Room;
import com.automated.house.command.repository.ResidenceRepository;
import com.automated.house.command.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "https://docenteprotagonista.com.br")
@RestController
public class RoomControllerImpl {

    @Autowired
    private RoomRepository repository;

    @GetMapping("/api/room/{id}")
    public ResponseEntity getRoom(@PathVariable("id") Long id){
        return repository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/api/room/create")
    public Room postRoom(@RequestBody Room room)
    {
        return repository.save(room);
    }

    @DeleteMapping("/api/room/delete/{cpf}")
    public ResponseEntity deleteRoom(@PathVariable("id") Long id)
    {
        try{
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        catch (Exception ex)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/api/room/update")
    public ResponseEntity updateRoom(@RequestBody Room room)
    {
        try{
            var value = repository.findById(room.getId());
            if(value.isEmpty())
            {
                return ResponseEntity.noContent().build();
            }
            else{
                value.get().setName(room.getName());
                repository.save(value.get());
            }
            return ResponseEntity.ok().body(value);
        }
        catch (Exception ex)
        {
            return ResponseEntity.internalServerError().build();
        }
    }
}