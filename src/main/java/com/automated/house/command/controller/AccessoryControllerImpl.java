package com.automated.house.command.controller;

import com.automated.house.command.domain.model.dto.Accessory;
import com.automated.house.command.domain.model.dto.Room;
import com.automated.house.command.repository.AccessoryRepository;
import com.automated.house.command.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "https://docenteprotagonista.com.br")
@RestController
public class AccessoryControllerImpl {

    @Autowired
    private AccessoryRepository repository;

    @GetMapping("/api/accessory/{id}")
    public ResponseEntity getAccessory(@PathVariable("id") Long id){
        return repository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/api/accessory/create")
    public Accessory postAccessory(@RequestBody Accessory accessory)
    {
        return repository.save(accessory);
    }

    @DeleteMapping("/api/accessory/delete/{cpf}")
    public ResponseEntity deleteAccessory(@PathVariable("id") Long id)
    {
        try{
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        catch (Exception ex)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/api/accessory/update")
    public ResponseEntity updateAccessory(@RequestBody Accessory accessory)
    {
        try{
            var value = repository.findById(accessory.getId());
            if(value.isEmpty())
            {
                return ResponseEntity.noContent().build();
            }
            else{
                value.get().setName(accessory.getName());
                repository.save(value.get());
            }
            return ResponseEntity.ok().body(value);
        }
        catch (Exception ex)
        {
            return ResponseEntity.internalServerError().build();
        }
    }
}