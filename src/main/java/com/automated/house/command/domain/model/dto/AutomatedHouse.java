package com.automated.house.command.domain.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class AutomatedHouse {
    private Residence residence;
    private List<RoomAccessory> roomList;
}
