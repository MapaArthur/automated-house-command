package com.automated.house.command.repository;

import com.automated.house.command.domain.model.dto.Accessory;
import com.automated.house.command.domain.model.dto.Room;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccessoryRepository extends CrudRepository<Accessory, Long> {

    @Query(value = "select * from accessory where id_room = :id ", nativeQuery = true)
    List<Accessory> findByIdRoom(Long id);
}
