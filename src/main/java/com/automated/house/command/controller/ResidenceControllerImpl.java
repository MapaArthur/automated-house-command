package com.automated.house.command.controller;

import com.automated.house.command.domain.model.dto.Residence;
import com.automated.house.command.domain.model.dto.User;
import com.automated.house.command.repository.ResidenceRepository;
import com.automated.house.command.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@CrossOrigin(origins = "https://docenteprotagonista.com.br")
@RestController
public class ResidenceControllerImpl {

    @Autowired
    private ResidenceRepository repository;

    @GetMapping("/api/residence/{id}")
    public ResponseEntity getResidence(@PathVariable("id") Long id) {
        return repository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/api/residence/create")
    public Residence postResidence(@RequestBody Residence residence) {
        return repository.save(residence);
    }

    @DeleteMapping("/api/residence/delete/{cpf}")
    public ResponseEntity deleteResidence(@PathVariable("id") Long id) {
        try {
            repository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/api/residence/update")
    public ResponseEntity updateResidence(@RequestBody Residence residence) {
        try {
            var value = repository.findById(residence.getId());
            if (value.isEmpty()) {
                return ResponseEntity.noContent().build();
            } else {
                value.get().setName(residence.getName());
                value.get().setCity(residence.getCity());
                value.get().setDistrict(residence.getDistrict());
                value.get().setRoad(residence.getRoad());
                value.get().setState(residence.getState());
                repository.save(residence);
            }
            return ResponseEntity.ok().body(value);
        } catch (Exception ex) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/api/residence/automated/{cpf}")
    public ResponseEntity getResidence(@PathVariable("cpf") String cpf) {
        var response = repository.findByCpf(cpf);
        if (response != null) {
            return ResponseEntity.ok().body(response);
        }
        else{
            return ResponseEntity.noContent().build();
        }
    }
}