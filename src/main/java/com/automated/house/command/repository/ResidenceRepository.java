package com.automated.house.command.repository;

import com.automated.house.command.domain.model.dto.AutomatedHouse;
import com.automated.house.command.domain.model.dto.Residence;
import com.automated.house.command.domain.model.dto.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ResidenceRepository extends CrudRepository<Residence, Long> {


    @Query(value = "select * from residence where cpf = :cpf ", nativeQuery = true)
    Residence findByCpf(@Param("cpf")String cpf);
}
