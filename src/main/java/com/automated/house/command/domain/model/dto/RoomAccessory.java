package com.automated.house.command.domain.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class RoomAccessory {
    private Room room;
    private List<Accessory> accessoryList;
}
