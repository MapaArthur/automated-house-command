package com.automated.house.command.controller;

import com.automated.house.command.domain.model.dto.AutomatedHouse;
import com.automated.house.command.domain.model.dto.Room;
import com.automated.house.command.domain.model.dto.RoomAccessory;
import com.automated.house.command.repository.AccessoryRepository;
import com.automated.house.command.repository.ResidenceRepository;
import com.automated.house.command.repository.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class AutomatedHouseControllerImpl {
    @Autowired
    private ResidenceRepository residenceRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private AccessoryRepository accessoryRepository;

    @GetMapping("/api/automatedHouse/{cpf}")
    public ResponseEntity getAccessory(@PathVariable("cpf") String cpf){
        AutomatedHouse automatedHouse = new AutomatedHouse();
        automatedHouse.setRoomList(new ArrayList<>());

        var residence = residenceRepository.findByCpf(cpf);
        automatedHouse.setResidence(residence);

        var room = roomRepository.findByIdResidence(residence.getId());
        for (Room rm: room) {
            RoomAccessory roomAccessory = new RoomAccessory();
            roomAccessory.setRoom(rm);
            roomAccessory.setAccessoryList(accessoryRepository.findByIdRoom(rm.getId()));
            automatedHouse.getRoomList().add(roomAccessory);
        }
        return ResponseEntity.ok().body(automatedHouse);
    }
}
